export const colorBlueDark = '#041D2F';
export const colorBlueAccent = '#07304C';
export const colorGreenPrimary = '#3DDB86';

//WebService
export const apiMovie = 'http://www.omdbapi.com/?s=avenger&apikey=997061b4&';
export const apiLogin = 'https://s1mple-tours-be.herokuapp.com/auth/client/login';


//AsycnStorage
export const IS_LOGIN_KEY = 'IS_LOGIN';
export const ACCOUNT_KEY = 'ACCOUNT_DATA';