import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';

import {colorBlueDark} from '../helper/constants';

function CardMovie(props) {
  const {year, title, imageUrl} = props;
  return (
    <View style={styles.baseItemCard}>
      <View style={styles.itemCardContainer}>
        <Text style={styles.titleCard}>{title}</Text>
        <View
          style={{
            justifyContent: 'flex-end',
            flexDirection: 'row',
            alignItems: 'center',
            position: 'absolute',
            end: 18,
            bottom: 14,
          }}>
          <Image source={require('../assets/date.png')} />
          <Text style={{color: 'white', marginStart: 8}}>{year}</Text>
        </View>
      </View>
      <Image style={styles.poster} source={{uri: imageUrl}} />
    </View>
  );
}

const styles = StyleSheet.create({
  poster: {
    height: 125,
    width: 80,
    borderRadius: 10,
    position: 'absolute',
    left: 12,
    top: 0,
  },
  baseItemCard: {
    height: 135,
    marginTop: 12,
    position: 'relative',
    justifyContent: 'flex-end',
  },
  itemCardContainer: {
    backgroundColor: colorBlueDark,
    height: 100,
    borderRadius: 10,
    paddingEnd: 12,
  },
  titleCard: {
    color: 'white',
    marginStart: 100,
    marginTop: 12,
    fontWeight: 'bold',
  },
});

export default CardMovie;
