import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
import axios from 'axios';

import CardMovie from '../../components/CardMovie';
import {colorBlueAccent, apiMovie, IS_LOGIN_KEY} from '../../helper/constants';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    responseMovie: {},
    listMovie: [],
  };

  componentDidMount() {
    axios.get(apiMovie).then((res) => {
      const responseMovie = res.data;
      this.setState({
        responseMovie: responseMovie,
        listMovie: responseMovie.Search,
      });
    });
  }

  onButtonLogoutClick(navigation) {
    this.clearAsyncStorage();
    navigation.replace('Login');
  }

  clearAsyncStorage = async () => {
    try {
      console.log('Clear AsyncStorage');
      await AsyncStorage.clear();
      await AsyncStorage.setItem(IS_LOGIN_KEY, 'false');
    } catch (e) {}
  };

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row'}}>
            <Image
              source={require('../../assets/user.png')}
              style={{height: 45, width: 45, marginRight: 18}}
            />
            <View style={{justifyContent: 'center'}}>
              <Text style={{color: 'white'}}>Hallo, Selamat Datang</Text>
              <Text style={{color: 'white'}}>Gilang Safera</Text>
            </View>
          </View>
          {/* button logout */}
          <TouchableOpacity
            onPress={() => {
              this.onButtonLogoutClick(navigation);
            }}>
            <View style={styles.logout}>
              <Text style={{fontWeight: 'bold', color: colorBlueAccent}}>
                Log out
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <Text style={styles.subSection}>Daftar Film</Text>
        <ScrollView>
          {this.state.listMovie.map((items, index) => {
            return (
              <View key={index}>
                <CardMovie
                  year={items.Year}
                  title={items.Title}
                  imageUrl={items.Poster}
                />
              </View>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorBlueAccent,
    paddingHorizontal: 16,
    paddingTop: 24,
  },
  subSection: {
    color: 'white',
    fontWeight: 'bold',
    marginTop: 54,
    marginBottom: 24,
  },
  logout: {
    backgroundColor: 'salmon',
    alignItems: 'center',
    justifyContent: 'center',
    height: 30,
    borderRadius: 20,
    paddingHorizontal: 21,
    alignSelf: 'center',
  },
});
