import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import {colorBlueAccent, IS_LOGIN_KEY} from '../../helper/constants';

let isLogin = 'false';

export default class SplashScreen extends Component {
  componentDidMount() {
    this.onGetData();
    setTimeout(() => {
      console.log('setTimeOut :', isLogin);
      isLogin === 'true'
        ? this.props.navigation.replace('Home')
        : this.props.navigation.replace('Login');
    }, 2000);
  }

  onGetData = async () => {
    try {
      const value = await AsyncStorage.getItem(IS_LOGIN_KEY);
      if (value === 'true') {
        isLogin = 'true';
      }
    } catch (e) {
      console.error('atSplashScreen:', e);
    }
  };
  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require('../../assets/salt.png')} />
        <Text style={styles.title}>SALT ACADEMY</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorBlueAccent,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo: {
    height: 120,
    width: 120,
  },
  title: {
    color: 'white',
    fontSize: 14,
    marginTop: 14,
  },
});
