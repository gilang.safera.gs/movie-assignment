import Axios from 'axios';
import React, {Component} from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {apiLogin, IS_LOGIN_KEY} from '../../helper/constants.js';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      usernameInput: '',
      passwordInput: '',
    };
  }

  handleButtonLogin() {
    this.showToast('Loading...');
    const {usernameInput, passwordInput} = this.state;
    const account = {
      username: usernameInput,
      password: passwordInput,
    };

    const requestBody = JSON.stringify(account);
    console.log('Cek JSON' + requestBody);
    Axios.post(apiLogin, requestBody, {
      headers: {
        'content-type': 'application/json',
      },
    })
      .then((response) => {
        console.log('Login Resopnse:' + JSON.stringify(response));
        this.storeData(response);
      })
      .catch(() => {
        console.log('Login Gagal');
        this.showToast('Pastikan Username dan Password benar');
      });
  }

  showToast(msg) {
    ToastAndroid.show(msg, ToastAndroid.SHORT);
  }

  storeData = async (response) => {
    let isLogin = 'false';
    if (response.data.token != null) {
      this.showToast('Login Berhasil');
      this.props.navigation.replace('Home');
      isLogin = 'true';
    } else {
      this.showToast('User tidak terdaftar');
    }
    try {
      await AsyncStorage.setItem(IS_LOGIN_KEY, isLogin);
    } catch (e) {
      // saving error
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{alignSelf: 'center', marginBottom: 42}}>
          <Image source={require('../../assets/salt_logo.png')} />
        </View>
        <View style={styles.inputText}>
          <TextInput
            onChangeText={(usernameText) => {
              this.setState({usernameInput: usernameText});
            }}
            placeholder="Username"
            placeholderTextColor="#10735C"
            style={{color: 'white'}}
          />
        </View>
        <View style={styles.inputText}>
          <TextInput
            onChangeText={(passwordText) => {
              this.setState({passwordInput: passwordText});
            }}
            secureTextEntry={true}
            placeholder="Password"
            placeholderTextColor="#10735C"
            style={{color: 'white'}}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            this.handleButtonLogin();
            // this.props.navigation.replace('Home');
          }}>
          <View style={styles.btnLogin}>
            <Text style={{fontSize: 16, fontWeight: 'bold', color: 'white'}}>
              Login
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#07304C',
    flex: 1,
    padding: 32,
  },
  inputText: {
    borderWidth: 2,
    borderColor: '#3DDB86',
    borderRadius: 6,
    marginTop: 12,
    paddingHorizontal: 12,
  },
  btnLogin: {
    width: 140,
    height: 40,
    backgroundColor: '#3DDB86',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-end',
    borderRadius: 6,
    marginTop: 12,
  },
});
