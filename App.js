import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import HomeScreen from './src/Pages/HomeScreen';
import LoginScreen from './src/Pages/LoginScreen';
import SplashScreen from './src/Pages/SplashScreen';

const Stack = createStackNavigator();
const hideHeader = {headerShown: false};
export default class App extends Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Splash">
          <Stack.Screen
            name="Splash"
            component={SplashScreen}
            options={hideHeader}
          />
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={hideHeader}
          />
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={hideHeader}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
